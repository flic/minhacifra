(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('NetworkError', NetworkErrorDialog);

	function NetworkErrorDialog ($mdDialog) {
		
		var errDialog = $mdDialog.alert()
					        .parent(angular.element(document.body))
					        .clickOutsideToClose(true)
					        .title('Ocorreu um erro de rede.')
					        .textContent('Não foi concluir a ação. Por favor, tente novamente mais tarde.')
					        .ariaLabel('Network Error')
					        .ok('OK');

		activate();
		return {
			show: show
		};
		
		////////////////////////////
		function activate() {

		}

		function show () {
			$mdDialog.show(errDialog);
		}
	}
})();