(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('SidenavService', SidenavService);

	function SidenavService ($mdSidenav) {
		activate();
		return {
			toggleLeft: buildToggler('sidenav-left'),
			closeLeft: buildCloser('sidenav-left')
		};
		
		////////////////////////////
		function activate() {

		}

		function buildToggler(navID) {
	      return function() {
	        $mdSidenav(navID)
	          .toggle();
	      };
	    }

	    function buildCloser (navID) {
	    	return function() {
	        $mdSidenav(navID)
	          .close();
	      };
	    }
	}
})();