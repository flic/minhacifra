(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('HomeCtrl', HomeCtrl);

	function HomeCtrl (MusicService) {
		var vm = this;
		
		////////////////////////////
		activate();

		function activate() {
			MusicService.findAll({limit: 20, include_docs: true}).then(function(response) {
				vm.top20Songs = [];
				vm.top20Songs.push(response.rows.slice(0, 10));
				vm.top20Songs.push(response.rows.slice(10, 20));
			});			
		}
		
	}
})();