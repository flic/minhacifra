(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('ArtistViewCtrl', ArtistViewCtrl);

	function ArtistViewCtrl (NetworkError, $rootScope, PouchService, $q, $scope, $state, $stateParams, lodash, CouchResource, $mdBottomSheet, $interval) {
		var vm = this;
		
		vm.artist = null;
		vm.musicCount = null;
		vm.offlineAccess = null;
		vm.replicating = null;

		vm.showOptions = showOptions;
		vm.toggleOfflineAccess = toggleOfflineAccess;

		////////////////////////////
		activate();
		function activate() {
			loadArtist($stateParams.id);
			retrieveArtistOfflineInfo();

			$rootScope.$on('startReplication', function (evt, data) {
				vm.replicating = true;
			});
			$rootScope.$on('finishReplication', function (evt, data) {
				vm.replicating = false;
				if(!$scope.$$phase) {
				  	$scope.$apply();
				}
			});
		}

		function loadArtist (id) {
			CouchResource.findOne(id)
			.then(function (response) {
				vm.artist = response;
				$state.current.data.title = vm.artist.name;
				loadMusics(id);
			});
		}

		function loadMusics (artistId) {
			vm.promise = CouchResource.find('music', {
				startkey: [artistId], 
				endkey: [artistId, {}]
			})
			.then(function (response) {
				vm.artist = vm.artist || {};

				vm.artist.musics = lodash.map(response.rows, function (value) {
					var music = {};
					music.name = value.key[1];
					music._id = value.key[2];
					return music;
				});

				vm.musicCount = response.rows.length;

				retrieveMusicsOfflineInfo();
			});
		}

		function showOptions () {
			$mdBottomSheet.show({
				templateUrl: 'artists/artist-options.bsheet.html',
				controller: 'ArtistViewCtrl as vm'
		    })
		    .then().finally(function () {
		    	retrieveArtistOfflineInfo();
		    	retrieveMusicsOfflineInfo();
		    });
		}

		function toggleOfflineAccess () {
			if (vm.offlineAccess)
				disableOfflineAccess();
			else enableOfflineAccess();
		}

		function enableOfflineAccess () {
			var docsToSync = lodash.flatten([vm.artist._id, lodash.map(vm.artist.musics, '_id')]);
			PouchService.addLocalDocs(docsToSync)
			.then(function (resp) {
				vm.offlineAccess = true;
			}, NetworkError.show);
		}

		function disableOfflineAccess () {
			var docsToSync = lodash.flatten([vm.artist._id, lodash.map(vm.artist.musics, '_id')]);
			PouchService.removeLocalDocs(docsToSync)
			.then(function (resp) {
				vm.offlineAccess = false;
			}, NetworkError.show);
		}

		function retrieveArtistOfflineInfo () {
			PouchService.getOfflineInfo($stateParams.id)
			.then(function (response) {
				vm.offlineAccess = response.available;
			});
		}

		function retrieveMusicsOfflineInfo () {
			lodash.forEach(vm.artist.musics, function (music) {
				PouchService.getOfflineInfo(music._id)
				.then(function (response) {
					music.offlineAccess = response.available;
				});
			});
		}
	}
})();