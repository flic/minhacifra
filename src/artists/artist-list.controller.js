(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('ArtistListCtrl', ArtistListCtrl);

	function ArtistListCtrl (PouchService, CouchResource, lodash) {
		var vm = this;
		
		vm.artists = [];
		vm.offlineOnly = false;

		vm.query = {
			limit: 10,
			page: 1,
			include_docs: true
		};

		vm.onPaginate = onPaginate;
		vm.toggleOfflineOnly = toggleOfflineOnly;

		////////////////////////////
		activate();
		function activate() {
			loadArtists();
		}

		function onPaginate (page, limit) {
			vm.query.skip = (page - 1) * limit;
			loadArtists();
		}

		function loadArtists () {
			vm.promise = CouchResource.find('artist', vm.query).then(function (response) {
				vm.artistCount = response.total_rows;
				vm.artists = lodash.map(response.rows, 'doc');
				retrieveArtistsOfflineInfo();
			});
		}		

		function retrieveArtistsOfflineInfo () {
			lodash.forEach(vm.artists, function (artist) {
				PouchService.getOfflineInfo(artist._id)
				.then(function (response) {
					artist.offlineAccess = response.available;
				});
			});
		}

		function toggleOfflineOnly () {
			if (vm.offlineOnly)
				disableOfflineOnly();
			else enableOfflineOnly();
			loadArtists();
		}

		function disableOfflineOnly () {
			vm.query.offline = false;
			vm.offlineOnly = false;
		}

		function enableOfflineOnly () {
			vm.query.offline = true;
			vm.offlineOnly = true;
		}
	}
})();