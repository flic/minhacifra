(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('PouchService', PouchService);

	function PouchService ($rootScope, $http, SERVER_URL, $q, lodash) {
		var localDB = null;
		var remoteDB = null;

		activate();
		return {
			setRemoteDB: setRemoteDB,
			getRemoteDB: getRemoteDB,
			put: put,
			post: post,
			get: get,
			query: query,
			allDocs: allDocs,
			addLocalDoc: addLocalDoc,
			addLocalDocs: addLocalDocs,
			removeLocalDoc: removeLocalDoc,
			removeLocalDocs: removeLocalDocs,
			getOfflineInfo: getOfflineInfo
		};
		
		////////////////////////////
		function activate() {
			localDB = new PouchDB('minhacifra', {auto_compaction: true});

			localDB.get('org.couchdb.user:monk_localUserMeta')
			.then(function () {
				replicateDesignDocs();
				watchLocalChanges();
			}, function (err) {
				localDB.put({
					_id: 'org.couchdb.user:monk_localUserMeta',
					localDocs: []
				});
				replicateDesignDocs();
				watchLocalChanges();
			});
		}

		function replicateDesignDocs () {
			PouchDB.replicate(remoteDB, localDB, {
				live: false, 
				retry: false, 
				doc_ids: ['_design/artist', '_design/music', '_design/playlist', '_design/search']
			});
		}

		function watchLocalChanges () {
			localDB.changes({
				since: 'now',
				live: true,
				doc_ids: ['org.couchdb.user:monk_localUserMeta'],
				include_docs: true
			})
			.on('change', function(change) {
				replicateLocalDocs();
			}).on('complete', function(info) {
				
			}).on('error', function (err) {
				
			});
		}

		function get (docID) {
			return $q(function(resolve, reject) {
				getAvailableDB()
				.then(function  (db) {
					db.get(docID)
					.then(resolve, reject);
				});
			});
		}

		function put (doc) {
			return $q(function(resolve, reject) {
				remoteDB.put(doc)
				.then(resolve, reject);
			});
		}

		function post (doc) {
			return $q(function(resolve, reject) {
				remoteDB.post(doc)
				.then(resolve, reject);
			});
		}

		function query (view, map, options) {
			return $q(function(resolve, reject) {
				getAvailableDB()
				.then(function  (db) {
					if (options.offline)
						db = localDB;
					delete options.offline;
					db.query(view + '/' + map, options)
					.then(resolve, reject);
				});
			});
		}

		function allDocs (options) {
			return $q(function(resolve, reject) {
				getAvailableDB()
				.then(function  (db) {
					db.allDocs(options)
					.then(resolve, reject);
				});
			});
		}

		function addLocalDoc (docId) {
			return $q(function (resolve, reject) {
				if (typeof docId !== undefined && docId && docId !== '') {
					localDB.get('org.couchdb.user:monk_localUserMeta').then(function (userMeta) {
						if (userMeta.localDocs.indexOf(docId) === -1) {
							userMeta.localDocs.push(docId);
							localDB.put(userMeta)
							.then(resolve, reject);
						} 
						else {
							resolve(userMeta);
						}
					}, reject);
				}
				else reject('invalid doc id');
			});
		}

		function addLocalDocs (docIds) {
			if (lodash.isUndefined(docIds) || !docIds || docIds === [])
				return $q(function (resolve, reject) {
					reject('invalid parameter');
				});
			return $q(function (resolve, reject) {
				localDB.get('org.couchdb.user:monk_localUserMeta').then(function (userMeta) {
					userMeta.localDocs = lodash.union(userMeta.localDocs, docIds);
					localDB.put(userMeta).then(resolve, reject);
				}, reject);
			});
		}

		function removeLocalDoc (docId) {
			return $q(function (resolve, reject) {
				if (typeof docId !== undefined && docId && docId !== '') {
					localDB.get('org.couchdb.user:monk_localUserMeta').then(function (userMeta) {
						userMeta.localDocs.splice(userMeta.localDocs.indexOf(docId), 1);
						localDB.put(userMeta)
						.then(resolve, reject);
					});
				}
			});
		}

		function removeLocalDocs (docIds) {
			if (lodash.isUndefined(docIds) || !docIds || docIds === [])
				return $q(function (resolve, reject) {
					reject('invalid parameter');
				});
			return $q(function (resolve, reject) {
				localDB.get('org.couchdb.user:monk_localUserMeta').then(function (userMeta) {
					userMeta.localDocs = lodash.difference(userMeta.localDocs, docIds);
					localDB.put(userMeta).then(resolve, reject);
				}, reject);
			});
		}

		function getOfflineInfo (docId) {
			var deferred = $q.defer();

			var info = {};
			localDB.get('org.couchdb.user:monk_localUserMeta')
			.then(function (userMeta) {
				if (userMeta.localDocs.indexOf(docId) !== -1) {
					info.available = true;
					deferred.resolve(info);
				} 
				else {
					info.available = false;
					deferred.resolve(info);
				}
			}, deferred.reject);

			return deferred.promise;
		}

		function replicateLocalDocs () {
			$rootScope.$emit('startReplication');
			localDB.get('org.couchdb.user:monk_localUserMeta').then(function (userMeta) {
				PouchDB.replicate(remoteDB, localDB, {live: false, retry: false, doc_ids: userMeta.localDocs})
				.on('complete', function (info) {
					$rootScope.$emit('finishReplication', info);
				});
			});
		}

		function setRemoteDB (db) {
			remoteDB = db;
		}

		function getRemoteDB () {
			return remoteDB;
		}

		function getAvailableDB () {
			return $q(function (resolve, reject) {
				$http.head(SERVER_URL)
				.then(function (result) {
					resolve(remoteDB);
				}, function (err) {
					resolve(localDB);
				});
			});
		}
	}
})();