(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('PlaylistAddCtrl', PlaylistAddCtrl);

	function PlaylistAddCtrl ($mdDialog) {
		var vm = this;
		
		vm.cancel = cancel;
		vm.answer = answer;

		////////////////////////////
		activate();
		function activate() {
			
		}

		function cancel () {
			$mdDialog.cancel();
		}

		function answer (response) {
			$mdDialog.hide(response);
		}
	}
})();