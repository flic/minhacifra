(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('PlaylistViewCtrl', PlaylistViewCtrl);

	function PlaylistViewCtrl (CouchResource, PlaylistService, $state, $stateParams, $q, $timeout, $interval, $scope, $mdToast, $http, $mdDialog, lodash, dragulaService, $mdBottomSheet, PouchService, NetworkError) {
		var vm = this;
		
		vm.playlist = null;
		vm.offlineAccess = null;

		vm.showOptions = showOptions;
		vm.toggleOfflineAccess = toggleOfflineAccess;
		vm.removeFromPlaylist = removeFromPlaylist;
		vm.removePlaylist = removePlaylist;

		////////////////////////////
		activate();
		function activate() {
			loadPlaylist($stateParams.id);

			dragulaService.options($scope, 'sixth-bag', {
				moves: function (el, container, handle) {
					return handle.className === 'mdi mdi-drag draggable';
				}
		    });

		    $scope.$on('sixth-bag.drop-model', function (e, el) {
			    updateServerOrder();
			});
		}

		function loadPlaylist (id) {
			PlaylistService.find(id)
			.then(function (response) {
				vm.playlist = response;
				retrieveMusicsOfflineInfo();
				retrievePlaylistOfflineInfo();
			});
		}

		function updateServerOrder () {
			var playlist = angular.extend({}, vm.playlist);
			playlist.musics = lodash.map(vm.playlist.musics, 'id');

			PlaylistService.update(playlist)
			.then(function (response) {
				vm.playlist._rev = response.rev;
			}, NetworkError.show);
		}

		function removeFromPlaylist (musicId) {
			var playlist = angular.extend({}, vm.playlist);
			// remove music with music ID
			playlist.musics = lodash.filter(playlist.musics, function (v) {
				return v.id.toString() !== musicId.toString();
			});
			//set musics array to database format
			playlist.musics = lodash.map(playlist.musics, 'id');
			
			PlaylistService.update(playlist)
			.then(function (response) {
				vm.playlist._rev = response.rev;
				vm.playlist.musics = lodash.filter(vm.playlist.musics, function (v) {
					return v._id.toString() !== musicId.toString();
				});
			}, NetworkError.show);
		}

		function removePlaylist (ev) {
			var confirm = $mdDialog.confirm()
									.title('Deseja excluir a playlist ' + vm.playlist.name + '?')
									.textContent('Atenção: esta operação não poderá ser desfeita.')
									.ariaLabel('Exclude Playlist')
									.targetEvent(ev)
									.ok('OK')
									.cancel('Cancelar');
		    $mdDialog.show(confirm).then(function() {
		    	CouchResource.findOne(vm.playlist._id)
		    	.then(function (response) {
		    		var playlist = response;
		    		playlist._deleted = true;

		    		PlaylistService.update(playlist)
		    		.then(function (response) {
		    			$mdToast.show($mdToast.simple().textContent('Playlist '+vm.playlist.name+' removida com sucesso.'));
		    			$scope.$emit('playlistDeleted');
		    			$state.go('app.home');
		    			$mdBottomSheet.hide();
		    		}, NetworkError.show);
		    	}, NetworkError.show);
		    });
		}

		function showOptions () {
			$mdBottomSheet.show({
				templateUrl: 'playlists/playlist-options.bsheet.html',
				controller: 'PlaylistViewCtrl as vm'
		    })
		    .then().finally(function () {
		    	retrievePlaylistOfflineInfo();
		    	retrieveMusicsOfflineInfo();
		    });
		}

		function toggleOfflineAccess () {
			if (vm.offlineAccess)
				disableOfflineAccess();
			else enableOfflineAccess();
		}

		function disableOfflineAccess () {
			vm.offlineAccess = false;
			var docsToSync = lodash.flatten([vm.playlist._id, lodash.map(vm.playlist.musics, 'id')]);
			PouchService.removeLocalDocs(docsToSync)
			.then(function (resp) {
				vm.offlineAccess = false;
			}, NetworkError.show);
		}

		function enableOfflineAccess () {
			vm.offlineAccess = true;
			var docsToSync = lodash.flatten([vm.playlist._id, lodash.map(vm.playlist.musics, 'id')]);
			lodash.forEach(docsToSync, function (doc) {
				if (doc.type === 'music_revision')
					docsToSync.push(doc.music);
			});
			PouchService.addLocalDocs(docsToSync)
			.then(function (resp) {
				vm.offlineAccess = true;
			}, NetworkError.show);
		}

		function retrievePlaylistOfflineInfo () {
			PouchService.getOfflineInfo($stateParams.id)
			.then(function (response) {
				vm.offlineAccess = response.available;
			});
		}

		function retrieveMusicsOfflineInfo () {
			lodash.forEach(vm.playlist.musics, function (music) {
				PouchService.getOfflineInfo(music.id)
				.then(function (response) {
					music.offlineAccess = response.available;
				});
			});
		}
	}
})();