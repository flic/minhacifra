(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('PlaylistService', PlaylistService);

	function PlaylistService ($q, $http, PouchService, lodash, CouchResource, AuthService) {
		activate();
		return {
			find: find,
			findAll: findAll,
			create: create,
			update: update,
			nextMusic: nextMusic
		};
		
		////////////////////////////
		function activate() {

		}
		
		function nextMusic (musicId, playlistId,counter){
			return find(playlistId)
			.then(function(response) {
				for (var i = response.musics.length - 1; i >= 0; i--) {
					if(response.musics[i]._id===musicId){
						if(response.musics[i+counter])
							return response.musics[i+counter]._id;
					}
				}
				return response.musics[0]._id;
			});
		}

		function find (id) {
			return $q(function (resolve, reject) {
				CouchResource.findOne(id)
				.then(function (response) {
					var options = {
						keys: response.musics || [],
						include_docs: true
					};

					PouchService.allDocs(options)
					.then(function populateMusics (musicsResponse) {
						var validMusics = lodash.filter(musicsResponse.rows, 'doc._id');
						
						response.musics = lodash.map(validMusics, function (row) {
							var music = angular.extend({}, row.doc, {id: row.doc._id});
							return music;
						});

						var artists = {
							keys: lodash.map(response.musics, 'artist'),
							include_docs: true
						};
						
						PouchService.allDocs(artists)
						.then(function populateArtists (artistsResponse) {
							for (var i = 0; i < response.musics.length; i++)
								response.musics[i].artist = artistsResponse.rows[i].doc;
						})
						.then(function populateRevisions () {
							lodash.forEach(response.musics, function (music) {
								if (music.type === 'music_revision') {
									CouchResource.findOne(music.music)
									.then(function (resp) {
										music.name = resp.name;
									});
								}
							})
							resolve(response);
						});
						
					}, reject);
				}, reject);
			});
		}

		function findAll (query) {
			return CouchResource.find('playlist', query);
		}

		function create (playlist) {
			playlist = angular.extend({}, playlist, {type: 'playlist'});
			return $q(function (resolve, reject) {
				AuthService.getUser().then(function (user) {
					playlist.author = user._id;

					PouchService.post(playlist)
					.then(resolve, reject);
				});
			});			
		}

		function update (playlist) {
			playlist = angular.extend({}, playlist, {type: 'playlist', _id: playlist.id || playlist._id});
			delete playlist.id;
			return PouchService.put(playlist);
		}
	}
})();