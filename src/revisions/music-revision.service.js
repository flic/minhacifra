(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('MusicRevisionService', MusicRevisionService);

	function MusicRevisionService ($q, PouchService, lodash) {
		var seqId = 0;
		var revisions = [];

		activate();
		return {
			find: find,
			findAll: findAll,
			create: create,
			update: update
		};
		
		////////////////////////////
		function activate() {

		}

		function find (id) {
			return PouchService.get(id);
		}

		function findAll (query) {
			return $q(function (resolve, reject) {
				resolve(activities);
			});
		}

		function create (revision) {
			return PouchService.post(revision);
		}

		function update (revision) {
			return PouchService.put(revision);
		}
	}
})();