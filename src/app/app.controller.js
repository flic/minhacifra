(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('AppCtrl', AppCtrl);

	function AppCtrl (UserService, PlaylistService, AuthService, $mdToast, $timeout, $state, $scope, $rootScope, SidenavService, $mdDialog, $mdMedia, lodash) {
		var vm = this;

		vm.$state = $state;
		vm.toggleLeft = SidenavService.toggleLeft;
		vm.currentUser = null;

		vm.showUserDialog = showUserDialog;
		vm.showAddPlaylistDialog = showAddPlaylistDialog;
		vm.logout = logout;
		
		////////////////////////////
		activate();
		function activate() {
			$rootScope.$on('$stateChangeSuccess', 
				function(event, toState, toParams, fromState, fromParams, options){ 
					$timeout(function () {
						SidenavService.closeLeft();
					}, 1000);
				}
			);

			AuthService.getUser()
			.then(function (response) {
				if (response && response.name) 
					setupUserInfo(response.name, response._id);
				if (response && response._id)
					loadUserPlaylists(response._id);
			});

			$rootScope.$on('userLogin', function (evt, data) {
				AuthService.getUser()
				.then(function (response) {
					setupUserInfo(data.name, response._id);
					loadUserPlaylists(response._id);
				});
			});

			$rootScope.$on('userLogout', destroyUserInfo);
			$rootScope.$on('playlistDeleted', function(){
				loadUserPlaylists(vm.currentUser.id);
			});
		}

		function showUserDialog (ev) {
			UserService.showUserDialog({}, ev);
	  	}

	  	function showAddPlaylistDialog (ev) {
	  		return $mdDialog.show({
		    	templateUrl: 'playlists/playlist-add.dialog.html',
		    	parent: angular.element(document.body),
		    	targetEvent: ev,
		    	clickOutsideToClose:true,
		    	controller: 'PlaylistAddCtrl as vm',
		    	fullscreen: false
		    })
		    .then(function (playlist) {
		    	if (playlist) {
		    		PlaylistService.create(playlist)
		    		.then(function (resp) {
		    			playlist.id = resp.id;
		    			vm.currentUser.playlists.push(playlist);
		    			$mdToast.show(
						    $mdToast.simple()
						    .textContent('Playlist ' + playlist.name + ' criada com sucesso.')
						    .hideDelay(3000)
						);
						return true;
		    		}, function (err) {
		    			$mdDialog.show($mdDialog.alert()
					        .parent(angular.element(document.body))
					        .clickOutsideToClose(true)
					        .title('Ocorreu um erro de rede.')
					        .textContent('Não foi possível criar a playlist. Por favor, tente novamente mais tarde.')
					        .ariaLabel('Playlist Creation Error')
					        .ok('OK')
						);
						return false;
		    		});
		    	}
		    });
	  	}

	  	function logout () {
	  		AuthService.logout()
	  		.then(function (success) {
	  			
	  		}, function (err) {
	  			$mdDialog.show($mdDialog.alert()
				        .parent(angular.element(document.body))
				        .clickOutsideToClose(true)
				        .title('Ocorreu um erro de rede.')
				        .textContent('Não foi possível sair da sua conta. Por favor, tente novamente mais tarde.')
				        .ariaLabel('Login Error')
				        .ok('OK')
				);
	  		});
	  	}

	  	function setupUserInfo (name, id) {
	  		vm.currentUser = angular.extend({}, vm.currentUser);
			vm.currentUser.name = name;
			vm.currentUser.id = id;
	  	}

	  	function loadUserPlaylists (id) {
	  		var query = {
	  			startkey: [id],
	  			endkey: [id, {}]
	  		};
	  		PlaylistService.findAll(query)
	  		.then(function (response) {
	  			vm.currentUser.playlists = lodash.map(response.rows, function (row) {
	  				var playlist = {};
	  				playlist.name = row.key[1];
	  				playlist.id = row.key[2];
	  				return playlist;
	  			});
	  		});
	  	}

	  	function destroyUserInfo () {
	  		vm.currentUser = null;
			$scope.$apply();
	  	}
	}
})();