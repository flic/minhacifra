(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('MusicListCtrl', MusicListCtrl);

	function MusicListCtrl (PouchService, CouchResource, lodash) {
		var vm = this;
		
		vm.musics = [];
		vm.offlineOnly = null;

		vm.query = {
			limit: 50,
			page: 1,
			include_docs: true
		};

		vm.onPaginate = onPaginate;
		vm.toggleOfflineOnly = toggleOfflineOnly;

		////////////////////////////
		activate();
		function activate() {
			loadMusics();
		}

		function onPaginate (page, limit) {
			vm.query.skip = (page - 1) * limit;
			loadMusics();
		}

		function loadMusics () {
			vm.promise = CouchResource.find('music', angular.extend({}, vm.query)).then(function (response) {
				vm.musicCount = response.total_rows;
				vm.musics = lodash.map(response.rows, function (row) {
					var music = {};
					music._id = row.key[2];
					music.name = row.key[1];
					if (row.doc)
						music.artist = row.doc.name;
					return music;
				});
				retrieveMusicsOfflineInfo();
			});
		}

		function retrieveMusicsOfflineInfo () {
			lodash.forEach(vm.musics, function (music) {
				PouchService.getOfflineInfo(music._id)
				.then(function (response) {
					music.offlineAccess = response.available;
				});
			});
		}

		function toggleOfflineOnly () {
			if (vm.offlineOnly)
				disableOfflineOnly();
			else enableOfflineOnly();
			loadMusics();
		}

		function disableOfflineOnly () {
			vm.offlineOnly = false;
			vm.query.offline = false;
		}

		function enableOfflineOnly () {
			vm.offlineOnly = true;
			vm.query.offline = true;
		}
	}
})();