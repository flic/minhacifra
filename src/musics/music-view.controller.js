(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('MusicViewCtrl', MusicViewCtrl);

	function MusicViewCtrl (AuthService, NetworkError, $interval, $stateParams, $scope, lodash,
	 PlaylistService, PouchService, CouchResource, $mdBottomSheet, $state) {
		var vm = this;

		var currentMusicInstanceId = null;
		
		vm.music = null;
		vm.musicRevision = null;
		vm.user = null;
		vm.musicRevisions = [];
		vm.userRevisions = [];
		vm.artist = null;
		vm.playlistIndex = null;
		vm.offlineAccess = null;
		vm.fontSize = 14;

		vm.increaseFontSize = increaseFontSize;
		vm.decreaseFontSize = decreaseFontSize;
		vm.showAddToPlaylistMenu = showAddToPlaylistMenu;
		vm.addToPlaylist = addToPlaylist;
		vm.playlistHasMusic = playlistHasMusic;
		vm.toggleMusicAtPlaylist = toggleMusicAtPlaylist;
		vm.toggleOfflineAccess = toggleOfflineAccess;
		vm.showOptionsBottomSheet = showOptionsBottomSheet;
		vm.nextMusic = nextMusic;
		vm.previousMusic = previousMusic;
		vm.upTone = upTone;
		vm.downTone = downTone;

		vm.buildPlaylistIndex = buildPlaylistIndex;

		////////////////////////////
		activate();
		function activate() {
			vm.loading = true;
			vm.fontSize = 14;

			AuthService.getUser()
			.then(function (currentUser) {
				if ($stateParams.revId && $stateParams.revId !== '') {
					loadMusic($stateParams.id)
					.then(function () {
						CouchResource.findOne($stateParams.revId)
						.then(function (musicRevision) {
							vm.musicRevision = musicRevision;
							currentMusicInstanceId = vm.musicRevision._id;
							vm.loading = false;
						});
					});
					loadRevisions($stateParams.id, currentUser.name);
				} else {
					loadMusic($stateParams.id).then(function (music) {
						vm.music = music;
						currentMusicInstanceId = vm.music._id;
						vm.loading = false;
					});
					loadRevisions($stateParams.id, currentUser.name);
				}
			});
		}

		function showAddToPlaylistMenu ($mdOpenMenu, ev) {
			$mdOpenMenu(ev);
		}

		function addToPlaylist (playlistId) {
			CouchResource.findOne(playlistId)
			.then(function (response) {
				var playlist = response;
				playlist.musics = playlist.musics || [];
				playlist.musics.push(currentMusicInstanceId);
				PlaylistService.update(playlist)
				.then(function () {
					lodash.find(vm.playlistIndex, {id: playlistId}).currentMusic.contains = true;
				}, NetworkError.show);
			}, NetworkError.show);
		}

		function removeFromPlaylist (id) {
			CouchResource.findOne(id)
			.then(function (response) {
				var playlist = response;
				playlist.musics = playlist.musics || [];
				playlist.musics = lodash.remove(playlist.musics, {_id: currentMusicInstanceId});
				PlaylistService.update(playlist)
				.then(function () {
					lodash.find(vm.playlistIndex, {id: id}).currentMusic.contains = false;
				}, NetworkError.show);
			});
		}

		function toggleMusicAtPlaylist (playlist) {
			if (!playlist.currentMusic.contains)
				addToPlaylist(playlist.id);
			else 
				removeFromPlaylist(playlist.id);
		}

		function playlistHasMusic (playlistId, musicId) {
			var $promise = {};
			CouchResource.findOne(playlistId)
			.then(function (response) {
				if(lodash.includes(response.musics, musicId))
					$promise.contains = true;
				else $promise.contains = false;
			});
			return $promise;
		}

		function buildPlaylistIndex (playlists) {
			vm.buildingPlaylistIndex = true;
			vm.playlistIndex = [];
			lodash.forEach(playlists, function (value) {
				var playlist = angular.extend({}, value);
				playlist.currentMusic = playlistHasMusic(playlist.id, currentMusicInstanceId);
				vm.playlistIndex.push(playlist);
			});
			var intervalPromise = $interval(function () {
				var playlistsLoading = lodash.filter(vm.playlistIndex, lodash.isUndefined);

				if (lodash.isEmpty(playlistsLoading)) {
					$interval.cancel(intervalPromise);
					vm.buildingPlaylistIndex = false;
				}
			}, 300);
		}


		function toggleOfflineAccess () {
			if (vm.offlineAccess)
				disableOfflineAccess();
			else enableOfflineAccess();
		}

		function disableOfflineAccess () {
			vm.offlineAccess = false;
			PouchService.removeLocalDoc(currentMusicInstanceId);
		}

		function enableOfflineAccess () {
			vm.offlineAccess = true;
			if (vm.musicRevision)
				PouchService.addLocalDoc(vm.music._id);
			PouchService.addLocalDoc(currentMusicInstanceId)
			.then(function () {
				PouchService.addLocalDoc(vm.music.artist);
			}, NetworkError.show);
		}

		function showOptionsBottomSheet () {
			$mdBottomSheet.show({
				templateUrl: 'musics/music-options.bsheet.html',
				controller: 'MusicViewCtrl as vm'
			})
			.then(function () {
				
			}, function () {
				
			}).finally(function () {
				retrieveOfflineAccessInfo(currentMusicInstanceId);
			});
		}

		function loadMusic (id) {
			return CouchResource.findOne(id)
			.then(function (response) {
				vm.music = response;
				retrieveOfflineAccessInfo(currentMusicInstanceId);
				return response;
			})
			.then(function (response) {
				loadArtist(vm.music.artist);
				return response;
			});
		}

		function retrieveOfflineAccessInfo (musicId) {
			PouchService.getOfflineInfo(musicId)
			.then(function (response) {
				vm.offlineAccess = response.available;
			});
		}

		function loadArtist (id) {
			CouchResource.findOne(id)
			.then(function (response) {
				vm.artist = response;
			});
		}

		function loadRevisions (id, username) {
			CouchResource.find('revisions', {
				startkey: [id],
				endkey: [id, {}]
			}, 'music')
			.then(function (resp) {
				vm.musicRevisions = lodash.map(resp.rows, function (row) {
					var revision = {};
					revision._id = row.id;
					revision.author = row.key[1];
					return revision;
				});
				vm.userRevisions = lodash.reduce(vm.musicRevisions, function (result, value) {
					if (value.author === username)
						result.push(value);
					return result;
				}, []);
			});
		}
		function nextMusic() {
			var mId = $stateParams.id||$stateParams.revId;
			PlaylistService.nextMusic(mId,$stateParams.playlistId,1).then(function(nextMusic) {
				$state.go('app.music-view', {id: nextMusic, playlistId: $stateParams.playlistId});
			});
		}
		function previousMusic() {
			var mId = $stateParams.id||$stateParams.revId;
			PlaylistService.nextMusic(mId,$stateParams.playlistId,-1).then(function(previousMusic) {
				$state.go('app.music-view', {id: previousMusic, playlistId: $stateParams.playlistId});
			});
		}
		function upTone(){
			var regexChords = /\[(\S+)\]/g;
			var content = vm.music.instruments.guitar.content;
			content = content.replace(regexChords, function (match, capture) {
				return '['+transposeChord(capture, 1)+']';
			});
			vm.music.instruments.guitar.content = content;
		}
		function downTone(){
			var regexChords = /\[(\S+)\]/g;
			var content = vm.music.instruments.guitar.content;
			content = content.replace(regexChords, function (match, capture) {
				return '['+transposeChord(capture, -1)+']';
			});
			vm.music.instruments.guitar.content = content;
		}
		function transposeChord(chord, amount) {
			var scale = [["C"], 
						 ["C#","Db"], 
						 ["D"], 
						 ["D#","Eb"], 
						 ["E"], 
						 ["F"], 
						 ["F#","Gb"],
						 ["G"], 
						 ["G#","Ab"],
						 ["A"], 
						 ["A#","Bb"],
						 ["B"]];
			return chord.replace(/[CDEFGAB](#|b)?/g, function(match) {
				for (var i = scale.length - 1; i >= 0; i--) {
					if(scale[i].indexOf(match)!=-1){
						var j = (i+amount) % scale.length;
						return scale[ j < 0 ? j + scale.length : j ][0];
					}
				}
			});
		}
		function increaseFontSize() {
			if(vm.fontSize<28)
				vm.fontSize=vm.fontSize+2;		}
		function decreaseFontSize() {
			if(vm.fontSize>8)
				vm.fontSize=vm.fontSize-2;
		}

	}
})();