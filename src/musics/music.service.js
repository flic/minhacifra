(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('MusicService', MusicService);

	function MusicService (CouchResource) {
		activate();
		return {
			findAll: findAll
		};
		
		////////////////////////////
		function activate() {

		}

		function findAll (query) {
			return CouchResource.find('music', query);
		}

	}
})();