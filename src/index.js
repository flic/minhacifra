(function() { 
	angular.module('minhacifra', ['outline', 
								  'templates',
								  'ui.router',
								  'ngMaterial',
								  'md.data.table',
								  'ngLodash',
								  'ngMessages',
								  'ui.ace',
								  'mcifraDecrypter',
								  angularDragula(angular)]); 
})();
