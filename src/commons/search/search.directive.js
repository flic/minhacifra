(function () {
	angular.module('minhacifra')
	.directive('mcSearch', function () {
		return {
			scope: {},
			bindToController: {

			},
			controller: McSearchCtrl,
			controllerAs: 'vm',
			templateUrl: 'commons/search/search.directive.html'
		};
	});

	function McSearchCtrl (CouchResource, $state, $timeout, $q, lodash) {
		var vm = this;

		vm.searchConfig = {};

		vm.states = loadAll();
		vm.querySearch = querySearch;

		vm.selectedItemChange = selectedItemChange;
		vm.searchTextChange = searchTextChange;

		vm.search = search;

		function search (searchTerm) {
			
		}

		function querySearch (queryStr) {
			//var results = queryStr ? vm.states.filter(createFilterFor(queryStr)) : vm.states;
			var results = [];

			var deferred;
			deferred = $q.defer();
			CouchResource.find('search', {
				include_docs: true,
				startkey: [queryStr],
				endkey: [queryStr.substring(0,queryStr.length-1)+String.fromCharCode(queryStr.charCodeAt(queryStr.length-1)+1), {}]
			}, 'music')
			.then(function (response) {
				results = lodash.map(response.rows, function (row) {
					var result = {};
					result.name = row.key[0];
					result.id = row.key[1];
					result.artist = row.doc.name;
					
					return result;
				});
				deferred.resolve(results);
			});
			return deferred.promise;
		}

		function searchTextChange (text) {
			
		}

		function selectedItemChange (item) {
			if (!item)
				return;
			$state.go('app.music-view', {id: item.id});
			vm.searchText = '';
		}

		function loadAll () {
			var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
              Wisconsin, Wyoming';
            
	      	return allStates.split(/, +/g).map( function (state) {
	        	return {
	          		value: state.toLowerCase(),
	          		display: state
	        	};
      		});
		}

		function createFilterFor(query) {
			var lowercaseQuery = angular.lowercase(query);
			return function filterFn(state) {
				return (state.value.indexOf(lowercaseQuery) === 0);
			};
	    }
	}
})();