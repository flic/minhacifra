(function() {

	angular.module("mcifraDecrypter", []);

	var mcifraDecrypter = function() {
		return {
			restrict: "E",
			scope: {
				"content": "@" 
			},
			replace: true,
			link: function(scope, elem, attrs) {

				var markdownMap = [
						[/\n/g, "<br>"],
						[/(<br>)+/g, "<br>"],
						[/\[\[/g, "<span class=\"mcifra-inline-chord\">"],
						[/\]\]/g, "</span>"],
						[/\]( ?)+\[/g, "&nbsp;"],			
						[/\[/g, "<span class=\"mcifra-block-chord\">"],
						[/\]/g, "</span>"],
						[/##=/g, "<div class=\"mcifra-comment-area\">"],
						[/#==/g, "<div class=\"mcifra-tab-area\">"],
						[/(=##|==#)/g, "</div>"],
						[/><br>/g, ">"]
					];

				var render = function() {

					angular.forEach(markdownMap, function(elem) {
						scope.content = scope.content.replace(elem[0], elem[1]);
					});
					
					elem.html("<div class=\"mcifra-panel-inner\">" + scope.content + "</div>");

					var tabAreas = angular.element(elem[0].querySelectorAll("[class='mcifra-tab-area']"));
					angular.forEach(tabAreas, function(area) {
						var matrix = angular.element(area).html().split("<br>");
						var matrixT = [];
						for (var i = 0; i < matrix.length; i++)
							for (var j = 0; j < matrix[i].length; j++) {
								if (matrixT.length == j) matrixT.push(["<div class=\"t-col\">"]);
								matrixT[j].push(matrix[i][j]);
							}
						for (var i = 0; i < matrixT.length; i++) {
							matrixT[i].push("</div>");
							matrixT[i] = matrixT[i].join("");
						}
						matrixT = matrixT.join("");
						angular.element(area).html(matrixT);
					});

				}
				
				scope.$watch("content", render);

			},
			template: "<div class=\"mcifra-panel\"></div>"
		}
	};

	angular.module("mcifraDecrypter").directive("mcifraDecrypter", mcifraDecrypter);

})();