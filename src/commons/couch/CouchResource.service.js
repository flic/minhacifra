(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('CouchResource', CouchResource);

	function CouchResource (PouchService, $http, $q, SERVER_URL) {
		activate();
		return {
			find: find,
			findOne: findOne
		};
		
		////////////////////////////
		function activate() {

		}

		function find (view, query, fn) {
			query = angular.extend({}, query);
			delete query.page;

			if (!fn)
				fn = 'list';

			return PouchService.query(view, fn, query);
		}

		function findOne (id) {
			return PouchService.get(id);
		}
	}
})();