(function() {

	var owlCarouselDirective = function() {
		return {
			restrict: "C",
			scope: {options: "="},
			link: function(scope, elem, attrs) {
				var renderCarousel = function() {
					$(elem[0]).owlCarousel(scope.options);
				};
				$(elem[0]).ready(renderCarousel);				
			}
		}
	};

	angular.module("minhacifra")
		.directive("owlCarousel", owlCarouselDirective);

})();