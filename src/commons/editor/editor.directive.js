(function () {
	
	angular.module('minhacifra')
	.directive('mcEditor', function () {
		return {
			scope: {},
			bindToController: {

			},
			controller: McEditorCtrl,
			controllerAs: 'vm',
			templateUrl: 'commons/editor/editor.directive.html'
		};
	});

	function McEditorCtrl (NetworkError, $mdToast, AuthService, UserService, MusicRevisionService, $rootScope, $state, $stateParams, $timeout, $q, CouchResource, lodash) {
		var vm = this;

		vm.musicRevision = null;
		vm.music = null;
		vm.addTabs = addTabs;
		vm.editor = null;
		vm.session = null;
		vm.preview = '';

		vm.saveRevision = saveRevision;

		vm.aceOptions = {
			onLoad: function (_ace) {
				_ace.getSession().setMode("ace/mode/cifra");
				vm.editor = _ace;
				vm.session = _ace.getSession();
				addCommands();
				activate();
			},
			onChange: function (e) {
				vm.preview = vm.editor.getValue();
				if(vm.changed){
					vm.changed=false;
				}else{
					lineUpperCase(e[0]);
					insertBracket(e[0]);
					removeBracket(e[0]);
				}
			},
			mode: 'cifra',
			require: ['ace/ext/language_tools'],
			advanced: {
				enableBasicAutocompletion: true,
				enableLiveAutocompletion: true,
				selectionStyle: "text"
			}
		};
		////////////////////////////
		function activate() {
			if ($stateParams.revId) {
				MusicRevisionService.find($stateParams.revId)
				.then(function (musicRevision) {
					vm.musicRevision = musicRevision;
					vm.session.insert(vm.editor.getCursorPosition(), musicRevision.instruments.guitar.content);
					vm.preview = musicRevision.instruments.guitar.content;
				}, function () {
					$state.go('app.music-edit', {revId: ''}, {notify: false});
				});

				loadMusic($stateParams.id);
			} else {
				AuthService.getUser()
				.then(function (user) {
					// Temporary revision object
					vm.musicRevision = {
						author: user.name,
						music: $stateParams.id,
						type: "music_revision"
					};
				}, function (err) {
					UserService.showUserDialog()
					.then(function (user) {
						vm.musicRevision = {
							author: user.name,
							music: $stateParams.id,
							type: "music_revision"
						};
					}, function () {
						$state.go('app.music-view', {id: $stateParams.id});
					});
				});

				loadMusic($stateParams.id).then(function (music) {
					vm.session.insert(vm.editor.getCursorPosition(), music.instruments.guitar.content);
					vm.preview = music.instruments.guitar.content;
				});
			}

			$rootScope.$on('userLogout', function () {
				$state.go('app.music-view', {id: $stateParams.id});
			});

			loadMusic($stateParams.id);
			vm.changed = false;
		}

		function addTabs() {
			vm.session.insert(vm.editor.getCursorPosition(),'#==\nE|--------\nC|--------\nD|--------\n#==');
		}

		function loadMusic (id) {
			return CouchResource.findOne(id)
			.then(function (response) {
				vm.music = response;
				return response;
			});
		}

		function saveRevision () {
			vm.musicRevision = angular.extend(vm.musicRevision, {
				instruments: {
					guitar: {
						content: vm.editor.getValue(),
						capo: vm.music.instruments.guitar.capo,
						key: vm.music.instruments.guitar.key
					}
				}
			});

			if ($stateParams.revId && $stateParams.revId !== '')
				vm.musicRevision._id = $stateParams.revId;

			var promise = null;
			if (vm.musicRevision._id)
				promise = MusicRevisionService.update(vm.musicRevision);
			else 
				promise = MusicRevisionService.create(vm.musicRevision);

			promise.then(function (resp) {
				vm.musicRevision._rev = resp.rev;
				vm.musicRevision._id = resp.id;
				$state.go('app.music-edit', {revId: resp.id}, {notify: false});
				$mdToast.show($mdToast
								.simple()
								.textContent('Suas alterações foram salvas com sucesso.'));
				console.log(vm.musicRevision);
			}, function (err) {
				console.log(err);
				NetworkError.show();
			});
		}

		function lineUpperCase (e) {
			var rng;
			if(e.action=="insert"&&e.lines[0].length<2&&e.lines[0]!=='['){
				if(e.start.column===0){
					if(e.lines[0] !== ""){
						vm.changed=true;
						var position = {
							row : e.start.row,
							column : e.start.column+1
						};
						rng = new Range(e.start.row, e.start.column, e.start.row, e.start.column+1);
						vm.editor.session.insert(position,e.lines[0].toUpperCase());
						vm.editor.session.remove(rng);
					}
				}
			}
		}
		function insertBracket(e){
			var position = {
				row : e.start.row,
				column : e.start.column+1
			};
			var rng;
			if(e.action=="insert"&&e.lines[0].length<2){
				if(e.lines[0] == "["){
					vm.changed=true;
					vm.editor.session.insert(position,']');
					vm.editor.moveCursorToPosition(position);
				}else if(e.lines[0]=="]"){
					rng = new Range(position.row, position.column, position.row, position.column+1);
					if(vm.editor.session.getTextRange(rng)==']') {
						changed=true;
						vm.editor.session.remove(rng);
					}
				}else{
					vm.changed=false;
				}
			}
		}
		function removeBracket (e) {
			var position = {
				row : e.start.row,
				column : e.start.column+1
			};
			var rng;
			if(e.action==="remove"&&e.lines[0] === "["){
				rng = new Range(position.row, position.column-1, position.row, position.column);
				if(vm.editor.session.getTextRange(rng)==']') {
					vm.changed=true;
					vm.editor.session.remove(rng);
				}
			}
		}
		function addChord(chord) {
			var position = vm.editor.getCursorPosition();
			vm.editor.session.insert(position, "["+chord+"]");
			position.column = position.column+1+chord.length;
			vm.editor.clearSelection();
			vm.editor.moveCursorToPosition(position);
			vm.editor.execCommand("startAutocomplete");
		}

		function addCommands(){
			vm.editor.commands.addCommand({
					name: 'addChordA',
					bindKey: {
					win: 'Alt-A',
					mac: 'Command-A',
					sender: 'editor|cli'
				},
				exec: function(env, args, request) {
					addChord("A");
				}
			});
			vm.editor.commands.addCommand({
				name: 'addChordB',
				bindKey: {
				win: 'Alt-B',
				mac: 'Command-B',
				sender: 'editor|cli'
			},
			exec: function(env, args, request) {
				addChord("B");
			}
			});
			vm.editor.commands.addCommand({
					name: 'addChordC',
					bindKey: {
					win: 'Alt-C',
					mac: 'Command-C',
					sender: 'editor|cli'
				},
				exec: function(env, args, request) {
					addChord("C");
				}
			});
			vm.editor.commands.addCommand({
					name: 'addChordD',
					bindKey: {
					win: 'Alt-D',
					mac: 'Command-D',
					sender: 'editor|cli'
				},
				exec: function(env, args, request) {
					addChord("D");
				}
			});
			vm.editor.commands.addCommand({
					name: 'addChordE',
					bindKey: {
					win: 'Alt-E',
					mac: 'Command-E',
					sender: 'editor|cli'
				},
				exec: function(env, args, request) {
					addChord("E");
				}
			});
			vm.editor.commands.addCommand({
					name: 'addChordF',
					bindKey: {
					win: 'Alt-F',
					mac: 'Command-F',
					sender: 'editor|cli'
				},
				exec: function(env, args, request) {
					addChord("F");
				}
			});
			vm.editor.commands.addCommand({
					name: 'addChordG',
					bindKey: {
					win: 'Alt-G',
					mac: 'Command-G',
					sender: 'editor|cli'
				},
				exec: function(env, args, request) {
					addChord("G");
				}
			});
		}

	}
})();
