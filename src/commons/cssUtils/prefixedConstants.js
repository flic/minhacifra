(function () {
	angular.module('minhacifra')
	.constant('boxOrdinalGroup', (function () {
		var boxOrdinalGroupNames = ['-webkit-box-ordinal-group', 'box-ordinal-group', 'moz-box-ordinal-group'];

	    var whichBoxOrdinalGroup = function () {
	        var t,
	            el = document.createElement("fakeelement");

	        for (var i = 0; i < boxOrdinalGroupNames.length; i++) {
	        	var name = boxOrdinalGroupNames[i];
	        	if (el.style[name] !== undefined)
	        		return name;
	        }
	    };
		return whichBoxOrdinalGroup();
	})())
	.constant('transitionEnd', (function () {
		var transitions = {
	        "transition"      : "transitionend",
	        "OTransition"     : "oTransitionEnd",
	        "MozTransition"   : "transitionend",
	        "WebkitTransition": "webkitTransitionEnd"
	    };

	    var whichTransitionEvent = function () {
	        var t,
	            el = document.createElement("fakeelement");

	        for (t in transitions) {
	            if (el.style[t] !== undefined){
	                return transitions[t];
	            }
	        }
	    };
		return whichTransitionEvent();
	})());
})();