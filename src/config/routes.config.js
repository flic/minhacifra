(function(){
	angular.module('minhacifra')
	.config(RoutesConfig);

	function RoutesConfig ($stateProvider, $urlRouterProvider, $locationProvider) {
		$stateProvider
		.state('app', {
			templateUrl: 'app/app.html',
			abstract: true,
			controller: 'AppCtrl as app'
		})
		.state('app.home', {
			url: '/home',
			templateUrl: 'home/home.html',
			controller: 'HomeCtrl as vm',
			data: {
				title: 'Home'
			}
		})
		.state('app.playlist-list', {
			url: '/playlist',
			templateUrl: 'playlists/playlist-list.html',
			data: {
				title: 'Playlists'
			}
		})
		.state('app.playlist-view', {
			url: '/playlist/:id',
			templateUrl: 'playlists/playlist-view.html',
			controller: 'PlaylistViewCtrl as vm',
			data: {
				title: 'Playlist'
			}
		})
		.state('app.playlist-edit', {
			url: '/playlist/:id/edit',
			templateUrl: 'playlists/playlist-edit.html',
			data: {
				title: 'Edição de Playlist'
			}
		})
		.state('app.music-list', {
			url: '/music',
			templateUrl: 'musics/music-list.html',
			controller: 'MusicListCtrl as vm',
			data: {
				title: 'Músicas'
			}
		})
		.state('app.music-view', {
			url: '/music/:id/:revId?playlistId',
			templateUrl: 'musics/music-view.html',
			controller: 'MusicViewCtrl as vm',
			data: {
				title: 'Música'
			}
		})
		.state('app.music-edit', {
			url: '/music/:id/edit/:revId',
			templateUrl: 'musics/music-edit.html',
			data: {
				title: 'Edição de Música'
			}
		})
		.state('app.artist-list', {
			url: '/artist',
			templateUrl: 'artists/artist-list.html',
			controller: 'ArtistListCtrl as vm',
			data: {
				title: 'Artistas'
			}
		})
		.state('app.artist-view', {
			url: '/artist/:id',
			templateUrl: 'artists/artist-view.html',
			controller: 'ArtistViewCtrl as vm',
			data: {
				title: 'Artista'
			}
		})
		.state('app.user-view', {
			url: '/user/:id',
			templateUrl: 'users/user-view.html',
			data: {
				title: 'Usuário'
			}
		})
		.state('app.user-edit', {
			url: '/user/:id/edit',
			templateUrl: 'users/user-edit.html',
			data: {
				title: 'Edição de Usuário'
			}
		});

		$urlRouterProvider.otherwise('/home');
		$locationProvider.html5Mode(true);
	}
})();