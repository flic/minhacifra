(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('UserService', UserService);

	function UserService ($q, $mdDialog, $mdMedia) {
		var userDialog = {
	    	templateUrl: 'users/user.dialog.html',
	    	parent: angular.element(document.body),
	    	clickOutsideToClose:true,
	    	controller: 'UserDialogCtrl as vm'
	    };

		activate();
		return {
			showUserDialog: showUserDialog
		};
		
		////////////////////////////
		function activate() {

		}

		function showUserDialog (options, ev) {
			var dialog = angular.extend(userDialog);
			var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));

			dialog.fullscreen = useFullScreen;

			if (ev)
				dialog.targetEvent = ev;

			return $mdDialog.show(dialog);
		}
	}
})();