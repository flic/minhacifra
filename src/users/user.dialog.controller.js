(function(){
	'use strict';

	angular.module('minhacifra')
	.controller('UserDialogCtrl', UserDialogCtrl);

	function UserDialogCtrl (AuthService, $mdDialog) {
		var vm = this;

		vm.loading = false;

		vm.hide = hide;
		vm.cancel = cancel;

		vm.signup = signup;
		vm.login = login;
		vm.clearUsernameErrors = clearUsernameErrors;

		var signupErrorDialog = $mdDialog.alert()
				        .parent(angular.element(document.body))
				        .clickOutsideToClose(true)
				        .title('Não foi possível cadastrar-se.')
				        .textContent('Por favor, tente novamente mais tarde.')
				        .ariaLabel('Signup Error')
				        .ok('OK');
		var signupSuccessDialog = $mdDialog.alert()
				        .parent(angular.element(document.body))
				        .clickOutsideToClose(true)
				        .title('Seja bem-vindo à comunidade!')
				        .textContent('Crie playlists, edite cifras e compartilhe à vontade.')
				        .ariaLabel('Signup Success')
				        .ok('OK');
		var loginErrorDialog = $mdDialog.alert()
				        .parent(angular.element(document.body))
				        .clickOutsideToClose(true)
				        .title('Não foi possível acessar sua conta.')
				        .textContent('Por favor, tente novamente mais tarde.')
				        .ariaLabel('Login Error')
				        .ok('OK');
		
		////////////////////////////
		activate();
		function activate() {
			
		}

		function signup (user) {
			vm.loading = true;
			AuthService.signup(user)
			.then(function (response) {
				vm.cancel();
				$mdDialog
					.show(signupSuccessDialog)
			        .finally(function() {
			        	AuthService.login(user);
			        });
			}, function (err) {
				vm.signupForm.username.$setValidity("username", false);
				if (err.name === 'conflict')
			    	vm.signupForm.username.$error.conflict = true;
			    else if (err.name === 'forbidden')
			    	vm.signupForm.username.$error.forbidden = true;
			    else
			    	$mdDialog.show(signupErrorDialog);
			}).finally(function () {
				vm.loading = false;
			});
		}

		function login (user) {
			vm.loading = true;
			AuthService.login(user)
			.then(function (response) {
				vm.hide(response);
			}, function (err) {
				vm.loginForm.username.$setValidity("username", false);
				if (err.name === 'unauthorized')
			    	vm.loginForm.username.$error.unauthorized = true;
			    else 
			    	$mdDialog.show(loginErrorDialog);
			}).finally(function () {
				vm.loading = false;
			});
		}

		function clearUsernameErrors () {
			delete vm.signupForm.username.$error.conflict;
			delete vm.signupForm.username.$error.forbidden;
			delete vm.loginForm.username.$error.conflict;
			delete vm.loginForm.username.$error.forbidden;
			vm.signupForm.username.$setValidity("username", true);
			vm.loginForm.username.$setValidity("username", true);
		}

		function hide (message) {
			$mdDialog.hide(message);
		}

		function cancel () {
			$mdDialog.cancel();
		}
		
	}
})();