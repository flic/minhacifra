(function(){
	'use strict';

	angular.module('minhacifra')
	.factory('AuthService', AuthService);

	function AuthService ($q, PouchService, $rootScope) {
		activate();
		return {
			signup: signup,
			login: login,
			logout: logout,
			getSession: getSession,
			getUser: getUser
		};
		
		////////////////////////////
		function activate() {

		}

		function signup (user) {
			return $q(function (resolve, reject) {
				if (!user.password || !user.username)
					reject('missing fields');
				var db = PouchService.getRemoteDB();
				if (!db)
					reject('remote database not set');
				db.signup(user.username, user.password, function (err, response) {
				  	if (err)
				  		reject(err);
				  	else 
				  		resolve(response);
				});
			});
		}

		function login (user) {
			return $q(function (resolve, reject) {
				if (!user.password || !user.username)
					reject('missing fields');
				var db = PouchService.getRemoteDB();
				if (!db)
					reject('remote database not set');
				db.login(user.username, user.password, function (err, response) {
				  	if (err)
				  		reject(err);
				  	else {
				  		$rootScope.$emit('userLogin', response);
				  		resolve(response);
				  	}
				});
			});
		}

		function getSession () {
			return PouchService.getRemoteDB().getSession();
		}

		function getUser () {
			return $q(function (resolve, reject) {
				getSession().then(function (response) {
					var username = "";

					if (response.userCtx.name)
						username = response.userCtx.name;
					else 
						reject('404');

					PouchService.getRemoteDB().getUser(username)
					.then(resolve, reject);
				});
			});
		}

		function logout () {
			var db = PouchService.getRemoteDB();
			return db.logout()
			.then(function (response) {
				$rootScope.$emit('userLogout', response);
				return response;
			}, function (err) {
				return err;
			});
		}
	}
})();