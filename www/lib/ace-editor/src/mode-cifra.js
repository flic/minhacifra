define('ace/mode/cifra', [], function(require, exports, module) {

var oop = require("ace/lib/oop");
var TextMode = require("ace/mode/text").Mode;
var Tokenizer = require("ace/tokenizer").Tokenizer;
var ExampleHighlightRules = require("ace/mode/cifra_highlight_rules").CifraHighlightRules;

var Mode = function() {
	this.HighlightRules = ExampleHighlightRules;
};
oop.inherits(Mode, TextMode);

(function() {
	//this.lineCommentStart = "--";
	this.blockComment = {start: "##", end: "##"};
}).call(Mode.prototype);

exports.Mode = Mode;
});
define('ace/mode/cifra_highlight_rules', [], function(require, exports, module) {

	var oop = require("ace/lib/oop");
	var TextHighlightRules = require("ace/mode/text_highlight_rules").TextHighlightRules;

	var CifraHighlightRules = function() {

	/*    var keywordMapper = this.createKeywordMapper({
			"variable.language": "",
			"keyword":
				"C|C#|D|D#|E|F|F#|G|A|A#|B",
			"constant.language":
				"false|null"
		}, "text", false);*/

		this.$rules = {
			"start": [
				{
					token: "markup.heading",
					regex: /\[\[/,
					next: [{
						regex: /\[/,
						token: "invalid",
						next: "start"
					}, {
						regex: /\]/,
						token: "markup.heading",
						next: [{
							regex:/[^]]/,
							token: "invalid",
							next: "start"
						},
						{
							regex:/\]/,
							token: "markup.heading",
							next: "start"
						},
						{
							defaultToken: "invalid"
						}]
					},{
						defaultToken: "markup.heading"
					}]
				},{
					regex: /\]/,
					token: "invalid",
					next: "start"
				},
				{
					token: "markup.heading",
					regex: /\[/,
					next: "cifra"
				},
				{
					token: "string",
					regex: '##=',
					next: [{
						token:"string",
						regex:"=##",
						next: "start"
					},{
						defaultToken: "string"
					}]
				}
			],
			"cifra": [
			{
					regex: /\]/,
					token: "markup.heading",
					next: "start"
				}, {
					regex: /\[/,
					token: "invalid",
					next: "start"
				},
				{
					defaultToken: "markup.heading"
				}
			]
		};
		this.normalizeRules();
	};

	oop.inherits(CifraHighlightRules, TextHighlightRules);

	exports.CifraHighlightRules = CifraHighlightRules;

	var langTools = ace.require("ace/ext/language_tools");
	var myCompleter = {
		getCompletions: function(editor, session, pos, prefix, callback) {
			callback(null, chordsJSON.map(function(chord) {
				var token = editor.session.getTokenAt(pos.row,pos.column);
				if(token)
					token= token.type;
				if(token==="markup.heading"){
					return {
						name: chord.chord,
						value: chord.value,
						meta: "acorde",
						score: chord.score
					};
				}
			}));
		}
	};
	langTools.setCompleters([myCompleter]);
});
var Range = require('ace/range').Range;

var chordsJSON = [
	{"chord":"C",   "value":"C",   "score":1000},
	{"chord":"Cm",  "value":"Cm",  "score":100},
	{"chord":"C#",  "value":"C#",  "score":1000},
	{"chord":"C#m",  "value":"C#m",  "score":100},
	{"chord":"D",   "value":"D",   "score":1000},
	{"chord":"Dm",   "value":"Dm",   "score":100},
	{"chord":"D#",  "value":"D#",  "score":1000},
	{"chord":"D#m",  "value":"D#m",  "score":100},
	{"chord":"E",   "value":"E",   "score":1000},
	{"chord":"Em",   "value":"Em",   "score":100},
	{"chord":"F",   "value":"F",   "score":1000},
	{"chord":"Fm",   "value":"Fm",   "score":100},
	{"chord":"F#",  "value":"F#",  "score":1000},
	{"chord":"F#m",  "value":"F#m",  "score":100},
	{"chord":"G",   "value":"G",   "score":1000},
	{"chord":"Gm",   "value":"Gm",   "score":100},
	{"chord":"G#",   "value":"G#",   "score":1000},
	{"chord":"G#m",   "value":"G#m",   "score":100},
	{"chord":"A",   "value":"A",   "score":1000},
	{"chord":"Am",   "value":"Am",   "score":100},
	{"chord":"A#",   "value":"A#",   "score":1000},
	{"chord":"A#m",   "value":"A#m",   "score":100},
	{"chord":"B",   "value":"B",   "score":1000},
	{"chord":"Bm",   "value":"Bm",   "score":100}
];