var path = require('path');
var express = require('express');

var app = express();

var staticPath = path.resolve(__dirname, 'www');
app.use(express.static(staticPath));

var ipaddress = process.env.OPENSHIFT_NODEJS_IP || 'localhost';
var port      = process.env.OPENSHIFT_NODEJS_PORT || '4759';

app.all('/*', function(req, res) {
    res.sendfile(path.resolve(__dirname, 'www/index.html'));
});

app.listen(port, ipaddress, function () {
  console.log('Static server listening at '+ipaddress+':'+port);
});